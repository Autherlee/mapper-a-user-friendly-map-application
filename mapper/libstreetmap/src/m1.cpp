#include "m1.h"

//load the map
bool load_map(std::string map_name) {

    bool load_success = loadStreetDatabaseBIN(map_name);

    // create any data structures here to speed up your API functions
    // ...

    return load_success;
}

//close the map
void close_map() {
    closeStreetDatabase();
    
    // destroy any data structures you created in load_map
    // ...
}

//implement the rest of the functions provided in m1.h
// ...